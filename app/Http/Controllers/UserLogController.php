<?php

namespace App\Http\Controllers;

use App\UserLog;
use Illuminate\Http\Request;

class UserLogController extends Controller
{
    public function index(Request $request)
    {
        $map = array();
        $ip = $request->input('ip');
        $browser = $request->input('browser');
        $device = $request->input('device');
        $os = $request->input('os');

        //按ip搜索
        if (filled($ip)) {
            $map['ip'] = $ip;
        };
        //浏览器
        if (filled($browser)) {
            $map['browser'] = $browser;
        };
        //设备
        if (filled($device)) {
            $map['device'] = $device;
        };
        //操作系统
        if (filled($os)) {
            $map['os'] = $os;
        };

        //按用户名搜索
        $username = $request->input('username');

        $userlogs = UserLog::with('user:id,name')
            ->where($map)
            ->when($username, function ($query) use ($username) {
                $query->whereHas('user', function ($query) use ($username) {
                    $query->where('name', $username);
                });

            })
            ->paginate(10);

        return view('userlog.index', compact('userlogs'));
    }
}
