<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## 关于此系统

#### 一个新冠期间基于社区的多商家版团购系统



>DEMO地址:http://ryss.nextauto.com.cn

>项目介绍地址:https://learnku.com/laravel/t/41494
>

项目安装后请复制下面代码并修改`user->is_admin` 添加超级管理员  然后使用超级管理员登录后台进行管理;
```php
a:1:{i:0;s:1:"0";}
```

#### 2020年3月21日起,武汉市商超将可以对个人进行开放, 本系统今日停止更新,愿病毒早日离去;

>本项目禁止任何商用行为！！！


#### 武汉加油!



