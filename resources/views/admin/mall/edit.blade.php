@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><a href="{{ route('admin.mall.index') }}"><a href="{{ route('admin.user.index') }}">商户管理</a> / 修改商户</a></div>
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="card-body">
                        <form action="{{route('admin.mall.update',$mall->id)}}" method="post">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="exampleInputPassword1">商户名称</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" name="name" value="{{ $mall->name }}" disabled >
                            </div>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="is_display" id="inlineRadio1"
                                           value="1" @if($mall->is_display==1) checked @endif>
                                    <label class="form-check-label" for="inlineRadio1">显示</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="is_display" id="inlineRadio2"
                                           value="0" @if($mall->is_display==0) checked @endif>
                                    <label class="form-check-label" for="inlineRadio2" >隐藏</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">提交</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
